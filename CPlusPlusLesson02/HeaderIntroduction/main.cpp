#include <cstdlib>
#include <stdio.h>
#include "OurMaths.h"
#include "MathUtility.h"
#include "Fibonacci.h"
#define print printf
#define runFibonacci(x) fibonacciSequence(0, 1, x);
#define PI 3.14
typedef float decimal;
typedef char* cString;
//int add(int x, int y);
//void printNumber(int value);

int main()
{
    cString myName = "Mou";
    print(myName);
    int number = add(5, 5);
    decimal pi = PI;
    printf("PI is %f\n", pi);
    printNumber(number);
    printNumber(multiply(3, 2));
    printTwoTimesTable();
    //fibonacciSequence(0, 1, 10);
    runFibonacci(10);
    system("PAUSE");
    
    return 0;
}

