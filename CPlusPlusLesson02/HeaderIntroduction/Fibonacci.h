//Fibonacci Sequence
//#pragma once
#ifndef __FIBONACCI_H__
#define __FIBONACCI_H__

#include "OurMaths.h" //we need to use add for this to work

void fibonacciSequence(int previousResult, int currentResult, int count)
{
    int newResult = add(previousResult, currentResult);
    printNumber(newResult);
    count--; //reduce the countdown since we've iterated once through here
    
    if(count > 0)
    {
        fibonacciSequence(currentResult, newResult, count);
    }
    else
    {
        printf("Done. \n");
    }
}

#endif
