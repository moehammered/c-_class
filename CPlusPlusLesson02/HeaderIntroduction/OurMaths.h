//#pragma once instruction to compiler to tell it only compile this file once
#ifndef __OURMATH_H__
#define __OURMATH_H__

int add(int x, int y)
{
    return x + y;
}

int multiply(int x, int y)
{
    return x * y;
}

void printNumber(int value)
{
    printf("Result is: %d\n", value);
}

#endif //end of the HEADER GUARD IF INSTRUCTION
