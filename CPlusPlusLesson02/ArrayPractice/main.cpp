#include <cstdlib>
#include <stdio.h>
void printNumber(int value);
void allocatePointer();
void setArrayValues(int value);

int numbers[10];
const int size = 3;
int changingSize = 1;
//Works because we have a constant variable
//This array is created at compile time! (because its in file scope)
bool boolArray[size]; 
//This doesn't work because changingSize is not constant
//float floatArray[changingSize];  //bad!
int* dynamicNumbersArray; //this is a pointer


int main()
{
    changingSize = 4;
    //works because this array is local to main
    //therefore it does not create this array at compile time!
    float floatArray[changingSize]; 
    numbers[0] = 5;
    *(numbers+0) = 6;
    *(numbers+3) = 26;
    //numbers[10] = 6;
    allocatePointer();
    dynamicNumbersArray[0] = 5;
    dynamicNumbersArray[1] = 3;
    setArrayValues(62);
    printf("Address: %x\n", numbers);
    printf("Address: %x\n", numbers+3);
    printNumber(numbers[0]);
    printNumber(numbers[3]);
    printNumber(*dynamicNumbersArray);
    printNumber(dynamicNumbersArray[1]);
    system("PAUSE");
    return 0;
}

void printNumber(int value)
{
    printf("Value: %d\n", value);
}

void allocatePointer()
{
     dynamicNumbersArray = new int[changingSize]; //allocates 2 memory location to the variable
     *dynamicNumbersArray = 0;
}

void setArrayValues(int value)
{
     for(int i = 0; i < changingSize; i++)
     {
         dynamicNumbersArray[i] = value;
     }
}












