#include "Input.h"

char convertToLowercase(char letter)
{
     //check if the letter is uppercase
     if(letter < 'a') //'a' == 97
     {
         //add 32 to the character to move it into lowercase zone
         letter += 32;
     }
     
     return letter;
}

bool validateInput(char input)
{
     //64 is the character before 'A' and 91 is the character after 'Z'
     if(input > 64 && input < 91)
     {
         //input is a capital letter
         return true; //return that this is a valid input
     } //96 is before 'a' and 123 is after 'z'
     else if(input > 96 && input < 123)
     {
          //input is a lowercase letter
         return true; //return that this is a valid input
     }
     
     return false; //no valid letter given
}

char getKeyboardInput()
{
     char letter = ' ';
     
     while(!validateInput(letter)) //while we don't have a valid letter
     {
         printString("Please input a letter and press Enter!");
         //wait for the user to give us a letter
         letter = getchar();
     }
     
     return convertToLowercase(letter); //valid input given, return it to be used 
}
