/**
   GameLogic Source
             Requirements
             -Lives count
             -String to represent a word
             -Array of booleans to represent guessed letters
*/


#include "GameLogic.h"

int wordSize = 5;
char* word = "hello";
//bool foundLetters[5] = {0, 0, 0, 0, 0}; //Fixed array
bool* foundLetters; //Dynamic pointer array
int lives = 5;

int readWord(char* newWord)
{
    int count = 0;
    
    while(newWord[count] != '\0')
    {
        count++;
    }
    
    return count;
}

void initialiseLettersArray(int length)
{
     //syntax to allocate a dynamic array
     //pointerName = new type[size];
     foundLetters = new bool[length];
     
     //IT IS IMPORTANT TO IMMEDIATELY INITIALISE YOUR ARRAY
     //Meaning go through the array after making it and set default values
     for(int i = 0; i < length; i++)
     {
         foundLetters[i] = false; //make all values false by default
     }
}

void initialiseGame(char* newWord, int livesCount)
{
     word = newWord;
     lives = livesCount;
     
     wordSize = readWord(newWord); //get how long the new word is and store it
     initialiseLettersArray(wordSize); //setup our foundLetters array
}

void runGame()
{
     printWelcomeMessage();
     
     while(lives > 0)
     {
         char guessedLetter = getKeyboardInput();
         if( compareLetter(guessedLetter, word, foundLetters) )
         {
             printCorrectGuess(guessedLetter);
             //has the word been complete?
             //loop through whole boolean array, if there is no false
             //player wins
             if(checkWin(word, foundLetters))
             {
                 //congrats we won!
                 //print win message!
                 printWin();
                 break; //stop the while loop, the game is over!
             }
         }
         else
         {
             //incorrect guess, display incorrect guess
             printIncorrectGuess(guessedLetter);
             //reduce lives
             lives--;
             //print remaining lives
             printLives(lives);
             //if their lives are == 0, they lose
             if(lives == 0)
             {
                 //we lose
                 //print lose message!
                 printLose();
             }
         }
         
         printCurrentStatus(word, foundLetters);
     }
}


void printCurrentStatus(char* word, bool* foundLetters)
{
     int count = 0;
     
     while(word[count] != '\0')
     {
         if(foundLetters[count] == true)
         {
             printf("%c", word[count]);
         }
         else
         {
             printf("_");
         }
         
         count++;
     }
     
     printf("\n");
}



bool checkWin(char* word, bool* foundLetters)
{
     int count = 0;
     
     while(word[count] != '\0')
     {
         if(foundLetters[count] == false)
         {
             return false; //a letter hasn't been guessed yet
         }
         count++;
     }
     
     return true; //all letters have been guessed
}
