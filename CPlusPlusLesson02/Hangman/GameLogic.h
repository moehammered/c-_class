
#ifndef __GAMELOGIC_H__
#define __GAMELOGIC_H__

#include "Display.h"
#include "Input.h"
#include "Comparer.h"

int readWord(char* newWord);
void initialiseLettersArray(int length);
void initialiseGame(char* newWord, int livesCount); //setup all the variables so they're ready for the game
void runGame();
bool checkWin(char* word, bool* foundLetters);
void printCurrentStatus(char* word, bool* foundLetters);

#endif
