/**
   Input Header
         Requirements
         -Display a message to user to accept input
         -Read input from the console
         -Return Validated Input from the console
         -Validate input from the console
         -Convert input to lowercase
*/


#ifndef __INPUT_H__
#define __INPUT_H__

#include "Display.h"

char convertToLowercase(char letter);
bool validateInput(char input);
char getKeyboardInput();

#endif
