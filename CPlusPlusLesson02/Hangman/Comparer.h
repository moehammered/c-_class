/**
   Comparer Header
            Requirements
            -Compare input from the console to a word
            -Compare a single letter to a whole word
            -Return the result of the comparison
            -Mark the positions of the letter where it was found in the word
                  eg "Hello" - 6 including '\0'
                     _ _ _ _ _ Booleans
                     if 'l' was guessed
                     x x o o x Booleans
*/

#ifndef __COMPARER_H__
#define __COMPARER_H__

bool compareLetter(char letter, char* word, bool* foundLetters);

#endif
