#include "Comparer.h"

bool compareLetter(char letter, char* word, bool* foundLetters)
{
     int count = 0; //index of the word
     bool result = false; //by default the comparison should be false
     
     while(word[count] != '\0') //'\0' is the end of a string
     {
         if(foundLetters[count] == false && word[count] == letter)
         {
             result = true;
             foundLetters[count] = true; //letter at this position was guessed
         }
         count++;
     }
     
     return result; //did we find any letters?
}
