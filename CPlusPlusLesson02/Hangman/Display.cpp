#include "Display.h"

void printString(char* string)
{
     printf("%s\n", string); //prints the string we give it followed by a new line
}

void printLives(int lives)
{
     printf("You have %d lives remaining!\n", lives);
}

void printWelcomeMessage()
{
     printString("Welcome to Hangman!");
     printString("You need to guess the letters to a word I'm hiding!");
     printString("Good Luck!!");
}

void printWin()
{
     printString("Congrats you guess the word correctly!");
}

void printLose()
{
     printString("Looks like you failed to guess the word!");
}

void printCorrectGuess(char letter)
{
     printf("Good! %c is a correct guess!\n", letter);
}

void printIncorrectGuess(char letter)
{
     printf("Bad! %c is an incorrect guess!\n", letter);
}
