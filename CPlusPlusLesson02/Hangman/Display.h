/**
   Display Header
           Requirements
           -Print a string to a console
           -Print lives remaining (print a string with numbers)
           -Print a welcome message
           -Print a winning message
           -Print a losing message
           -Print a correct guess message
           -Print an incorrect guess message
*/


#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include <cstdlib>
#include <stdio.h>

void printString(char* string);

void printLives(int lives);

void printWelcomeMessage();

void printWin();

void printLose();

void printCorrectGuess(char letter);

void printIncorrectGuess(char letter);

#endif
