/**
   Hangman Project
           Requirements
           -Keyboard input
           -comparison of letter to letters in a word
           -display/feedback
   
    //make an array and initialise the default values
    bool foundLetters[5] = {0, 0, 0, 0, 0};
    printWelcomeMessage();
    printString("This is a test!");
    printLives(4);
    printWin();
    printLose();
    printCorrectGuess('c');
    printIncorrectGuess('d');
    printf("Valid input tester: %i\n", validateInput('z'));
    printf("%c\n", getKeyboardInput());
    printf("Compare letter tester: %i\n", compareLetter('l', "hello", foundLetters));
    printf("Compare letter tester: %i\n", compareLetter('l', "hello", foundLetters));
*/

#include <cstdlib> //for printing and stuff
#include <stdio.h> //for keyboard input
/*#include "Display.h"
#include "Input.h"
#include "Comparer.h"*/
#include "GameLogic.h" //our game

bool checkPlayAgain();

bool quit = false; //should the game stop?

int main()
{
    while(!quit) //while we aren't quitting
    {
        initialiseGame("keyboard", 3);
        runGame();
        quit = checkPlayAgain();
        printf("Quit: %i\n", quit);
    }
    system("PAUSE");
    return 0;
}

bool checkPlayAgain()
{
     char letter = ' ';
     printf("Do you wish to play again? ");
     while(letter != 'y' && letter != 'n')
     {
         letter = getchar(); //keep asking for input until they give y or n
     }
     
     if(letter == 'y')
     {
         return false; //yes they want to play again(i.e don't want to quit)
     }
     else
     {
         return true; //no they want to quit
     }
}





