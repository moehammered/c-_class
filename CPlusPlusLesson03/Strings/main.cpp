//figuring out the length of a string (c style string)
/**
   Function called 'strlen' which can figure out the length of a string
   Use a loop to count the letters in a word until '\0' character is found
*/

#include <cstdlib>
#include <cstring>
#include <stdio.h>

void printStringLength(char* str);

int main()
{
    printStringLength("Test");
    printStringLength("Hi");
    printStringLength("This is actually a sentence!");
    
    system("PAUSE");
    return 0;
}

void printStringLength(char* str)
{
     //strlen method
     printf("strlen method. String length is: %i\n", strlen(str));
     //printf method
     int length = 0;
     length = printf("%s", str);
     printf("\nprintf method. String length is: %i\n", length);
     //looping method
     int count = 0;
     while(str[count] != '\0')
     {
         count++;
     }
     printf("loop null terminator check. String length is: %i\n", count);
}










