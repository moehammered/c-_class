//Memory allocation and de-allocation
/**
   It is important to keep track of the memory that we allocate 
   and de-allocate. Every program needs to ensure that it does not leave
   memory allocated when it no longer needs it. This is because RAM is 
   memory that is shared by every program, and if a program does not free
   the memory it no longer uses, then other programs cannot have proper
   access to it UNLESS the operating system or any other system is in place
   that will automatically cleanup unused memory.
   
   There is a term for this, and it is called 'garbage collection'. In C++
   there is no automatic garbage collection, if you allocate to the 'heap'
   then you must manually free that memory yourself. In languages like C#,
   Java, python, most scripting languages, garbage collection is automated.
*/

#include <cstdlib>
#include <stdio.h>

void recursion(int count);
void allocateArray(int length);
void deallocateArray(int length);
void printVariableSizes();
void setMemoryLocation();
void allocateMemory();
void printMemoryLocation(int* intPointer);
void printNumber(int value);

char* charArray;

int main()
{
    //setMemoryLocation();
    recursion(0);
    printVariableSizes();
    allocateMemory();
    char pressedKey = ' ';
    while(pressedKey != 'q') //keep looping until we type q in the console
    {
        //check the keypress
        pressedKey = getchar();
        if(pressedKey == 'a')
        {
            //allocate memory
            //20 megabytes = 20 * 1024 Kilobytes = 20 * 1024 * 1024 bytes
            //20971520 bytes == 20 megabytes
            int bytes = 1 * 1024 * 1024 * 1024;
            printf("Allocating memory\n");
            allocateArray(bytes);
        }
        else if(pressedKey == 'd')
        {
            //deallocate memory
            printf("De-Allocating memory\n");
            deallocateArray(20971520);
        }
    }
    
    system("PAUSE");
    return 0;
}

void allocateArray(int length)
{
     charArray = new char[length];
     printf("%i memory locations allocated.\n", length);
     printMemoryLocation((int*)charArray);
     
     for(int i = 0; i < length; i++)
     {
         charArray[i] = 'i';
     }
}

void deallocateArray(int length)
{
     //syntax for deleting a dynamically allocated array
     //delete [] pointerName; OR
     //delete [] arrayname; //same thing
     delete [] charArray;
     charArray = NULL; //remove the address it was pointing to
}

void setMemoryLocation()
{
    char* manualPointer = 0;
    manualPointer += 0x551708;
    
    printNumber(*manualPointer);
}

void allocateMemory()
{
    //when a pointer is == NULL it means that it no longer points to an address
    //IT DOES NOT MEAN IT IS DELETED OR THAT THE MEMORY IT POINTED TO IS FREE
    int* pointerTest = NULL;
    printMemoryLocation(pointerTest);
    
    //syntax for allocating memory to a pointer
    //pointerName = new type; //this will allocate 1 memory location
    pointerTest = new int;
    printMemoryLocation(pointerTest);
    
    //It is important to initialise the value of a pointer immediately
    //after allocating it memory
    //syntax to assign a value to a memory location a pointer is assigned to
    //*pointerName = value;
    *pointerTest = -55555; //initialised value
    printNumber(*pointerTest);
    
    //when we are done with a pointer or no longer need the memory or even
    //wish to change the amount of memory the pointer is assigned to, we
    //must first delete any memory it already is assigned to before doing
    //anything else. This is so that we free the memory the pointer was
    //given when we allocated it memory.
    //Syntax to deallocate memory or delete a pointer
    //delete pointerName;
    delete pointerTest; //this deletes the memory location it was assigned
    
    //IT IS IMPORTANT TO THEN NULLIFY THE ADDRESS THE POINTER IS REFERRING TO
    //this is so we don't accidentally try to use the deleted memory
    pointerTest = NULL;
}

void printNumber(int value)
{
     printf("Value is: %i\n", value);
}

void printMemoryLocation(int* intPointer)
{
     //Formatting ( 0x is the start of a hex memory address ) 
     //this is just for readability sake
     //Formatting ( %X means format the print to show a hex value all in UPPERCASE )
     //(\n is just to have a new line placed after the addess is printed)
     printf("Memory location of pointer is 0x%X\n", intPointer);
}


void printVariableSizes()
{
     //print the size of the following
     //bool, char, int, float, double, short, long
     printf("Char size ");
     printNumber(sizeof(char));
     printf("int size ");
     printNumber(sizeof(int));
     printf("bool size ");
     printNumber(sizeof(bool));
     printf("float size ");
     printNumber(sizeof(float));
     printf("double size ");
     printNumber(sizeof(double));
     printf("short size ");
     printNumber(sizeof(short));
     printf("long size ");
     printNumber(sizeof(long));
}


void recursion(int count)
{
     printf("Recurring %i\n", count);
     count++;
     recursion(count);
}






