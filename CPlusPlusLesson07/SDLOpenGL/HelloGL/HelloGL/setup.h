//Using SDL, SDL OpenGL, standard IO, and, strings
#include <SDL.h>
#include <SDL_opengl.h>
#include <GL\GLU.h>
#include <stdio.h>
#include <string>

//Starts up SDL, creates window, and initializes OpenGL
bool init(int SCREEN_WIDTH, int SCREEN_HEIGHT);

//Initializes matrices and clear color
bool initGL();

bool initGL()
{
	bool success = true;
	GLenum error = GL_NO_ERROR;

	//Initialize Projection Matrix
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//properties for a projection matrix
	GLdouble fieldOfView, aspectRatio, nearPlane, farPlane;
	fieldOfView = 45;
	aspectRatio = 800 / 600; //based on resolution
	nearPlane = 0.2;
	farPlane = 1000;
	//setup a projection matrix which uses 3D perspective viewing
	gluPerspective(fieldOfView, aspectRatio, nearPlane, farPlane);
	//Check for error
	error = glGetError();
	if (error != GL_NO_ERROR)
	{
		printf("Error initializing OpenGL! %s\n", gluErrorString(error));
		success = false;
	}

	//Initialize Modelview Matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//Check for error
	error = glGetError();
	if (error != GL_NO_ERROR)
	{
		printf("Error initializing OpenGL! %s\n", gluErrorString(error));
		success = false;
	}
	//Initialize clear color - initialise the colour the screen gets cleared to
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	//Check for error
	error = glGetError();
	if (error != GL_NO_ERROR)
	{
		printf("Error initializing OpenGL! %s\n", gluErrorString(error));
		success = false;
	}

	return success;
}