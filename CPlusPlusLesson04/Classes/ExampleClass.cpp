#include "ExampleClass.h"
#include <cstdlib>
#include <stdio.h>

/**
   -Global
          -File
               -Class
                     -Function
                              -Statement
*/

//'scope resolution' operator (::)

ExampleClass::ExampleClass()
{
    printWord("ExampleClass is constructed!");
    message = "This is a default message";
    name = "";
}

void ExampleClass::printHello()
{
     printWord(message);
     printWord(name);
}

void ExampleClass::printWord(char* word)
{
     printf("%s\n", word);
}

//deconstructor which is called when the object is destroyed
ExampleClass::~ExampleClass() 
{
    printWord("ExampleClass is deconstructed");
    delete message; //delete the message string pointer
    delete name; //delete the name string pointer
}




