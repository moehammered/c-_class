#include <cstdlib>
#include <stdio.h>
#include "ExampleClass.h"

int main()
{
    ExampleClass array[10];
    
    ExampleClass object;
    object.name = "My Name"; 
    object.printHello();
    
    ExampleClass object2;
    object2.name = "Another Name";
    object2.printHello();
    
    ExampleClass* dynamicObject;
    dynamicObject = NULL;
    
    char key = ' ';
    while(key != 'q')
    {
        key = getchar();
        if(key == 'a')
        {
               //create a new instance of the class
               //syntax of allocating memory to a pointer
               //pointerName = new type[size];
               //pointerName = new type (); //the () only for classes/objects
               dynamicObject = new ExampleClass();
               //this firstly derefences the pointer to find the object
               //in memory, and then you use the '.' to look inside the object
               (*dynamicObject).printHello();
               //However this can become very cumbersome so there is
               //a special operator which is called 'dereference and access'
               //This operator will dereference a pointer and then access
               //its members for you. The operator is '->'
               //syntax of using this on an object 
               //pointerName '->' memberFunction();
               //pointerName '->' memberVariable = ??;
               dynamicObject->printHello();
        }
        else if(key == 'd')
        {
             //delete the instance of the class
             delete dynamicObject;
        }
    }
    
    system("PAUSE");
    return 0;
}
