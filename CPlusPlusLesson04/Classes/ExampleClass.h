#ifndef __EXAMPLECLASS_H__
#define __EXAMPLECLASS_H__

class ExampleClass
{
    public://anything under here is considered public
        ExampleClass();
        char* name;
        void printHello();
        ~ExampleClass(); //tilde ~ denotes the deconstructor
        
    private://anything under here is considered private
        char* message;
        void printWord(char* word);
};

#endif
