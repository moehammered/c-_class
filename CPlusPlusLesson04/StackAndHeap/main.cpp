#include <cstdlib>
#include <stdio.h>
#include "heap.h"

//global variables are stored in the HEAP! NOT THE STACK!
const int globalSize = 1024 * 1024 * 1024;
char globalCharArray[globalSize];

int main()
{
    for(int i = 0; i < globalSize; i++)
    {
         globalCharArray[i] = 0;
    }
    //this variable is created on the stack.
    //That is because it is a local variable that belongs to the function main
    //it is also because you are not using the 'new' keyword.
    //meaning, the compiler doesn't have to guess that this variable will be
    //new, it will be a fixed variable of fixed address and size and it is 
    //allowed to control its memory.
    int bytes = 0;
    // bytes = megabytes amount * kilobyte conversion * byte conversion;
    bytes = 200 * 1024 * 1024;
    //this is on the stack
    char stackArray[1024*1486];
    char* heapArray = 0;
    char key = ' ';
    while(key != 'q')
    {
        key = getchar();
        if(key == 'a')
        {
            printf("Trying to allocate memory: %i bytes\n", bytes);
            if(heapArray == 0) //is the pointer null?
            {
                //if so then allocate memory
                heapArray = allocateToHeap(bytes);
            }
            else
            {
                printf("Pointer already has memory allocated to it. Please de-allocate first.\n");
            }
        }
        else if(key == 'd')
        {
            heapArray = deallocateFromHeap(heapArray);
            printf("Heap De-allocated. Memory location is now: %X\n", heapArray);
        }
    }
    
    system("PAUSE");
    return 0;
}
