#ifndef __HEAP_H__
#define __HEAP_H__

char* allocateToHeap(int bytes); //a char is 1 byte
char* deallocateFromHeap(char* array); //de-allocate memory pointers

#endif
