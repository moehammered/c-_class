#include "heap.h"
#include <cstdlib>
#include <stdio.h>

char* allocateToHeap(int bytes) //a char is 1 byte
{
      //syntax of allocating memory to a pointer
      //pointerName = 'new' 'type' '[length]' ';'
      char* memory = 0; //0 == NULL for a pointer
      
      //by using 'new' we are asking for memory from the heap
      memory = new char[bytes]; 
      
      for(int i = 0; i < bytes; i++)
      {
          memory[i] = 0;
      }
      
      return memory; //give the pointer to the new memory location
}

char* deallocateFromHeap(char* array)
{
     //syntax for de-allocating an array is
     //delete '[]' pointerName;
     delete [] array;
     //IT IS IMPORTANT TO MAKE THE POINTER NULL AFTER DELETING IT
     array = NULL;
     
     return array;
}




