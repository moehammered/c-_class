#include "ConsolePrinter.h"
#include <cstdlib>
#include <stdio.h>
#include <Windows.h> //windows sleep function Sleep(milliseconds)
#include <unistd.h> //unix sleep function usleep(milliseconds)

ConsolePrinter::ConsolePrinter()
{
    //default constructor
    typewriterMode = false;
}

ConsolePrinter::ConsolePrinter(bool slowType)
{
    //overloaded constructor
    typewriterMode = slowType;
}

void ConsolePrinter::printString(char* string)
{
    if(!typewriterMode)
        printf("%s\n", string);
    else
    {
        int characterIndex = 0;
        //start of sentence is here
        while(string[characterIndex] != '\0')
        {
            //current letter in the sentence
            printf("%c", string[characterIndex]);
            //need to delay the next character print
            //we'll fake it for now with a long loop.
            Sleep(100);
            characterIndex++; //move to next character in sentence
        }
        printf("\n"); //end of sentence is here
    }
}

void ConsolePrinter::printNumber(int value)
{
    printf("Number: %i\n", value);
}

ConsolePrinter::~ConsolePrinter()
{
    //default deconstructor
}
