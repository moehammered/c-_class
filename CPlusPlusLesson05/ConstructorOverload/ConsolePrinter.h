#ifndef __CONSOLEPRINTER_H__
#define __CONSOLEPRINTER_H__

class ConsolePrinter
{
    public:
        ConsolePrinter();
        ConsolePrinter(bool slowType);
        
        void printString(char* string);
        void printNumber(int value);
        
        ~ConsolePrinter();
    
    private:
        bool typewriterMode;
};

#endif
