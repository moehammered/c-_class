#include "Calculator.h"

Calculator::Calculator()
{
    //default constructor
}

int Calculator::add(int v1, int v2)
{
    return v1+v2;
}

int Calculator::sub(int v1, int v2)
{
    return v1-v2;
}

int Calculator::mult(int v1, int v2)
{
    return v1*v2;
}

int Calculator::divide(int v1, int v2)
{
    return v1/v2;
}

Calculator::~Calculator()
{
    //default deconstructor
}
