#ifndef __CALCULATOR_H__
#define __CALCULATOR_H__

class Calculator
{
    public:
        Calculator(); //constructor function
        
        int add(int v1, int v2);
        int sub(int v1, int v2);
        int mult(int v1, int v2);
        int divide(int v1, int v2);
        
        ~Calculator(); //deconstructor function
};

#endif
