#include <cstdlib>
#include <stdio.h>
#include "ConsolePrinter.h"
#include "Calculator.h"

int main()
{
    //Create a pointer of type 'Calculator'
    Calculator* calc;
    //assigning memory to the pointer by creating an instance
    //of the object and placing it in the heap
    //The pointer then has a memory address of the instance created.
    calc = new Calculator();
    //this creates an object and runs the default constructor
    ConsolePrinter printer(true); 
    
    printer.printString("Hello I'm a printer!");
    //a pointer must be dereferenced in order to get the object
    //the pointer is assigned to in memory. Only after dereferencing
    //can we access the class' attributes and behaviours. (*calc)
    int result = calc->add(12, 5); //(*calc).add(12,5); is equivalent
    printer.printNumber(result);
    
    //We should delete pointers once we're done with them because
    //they don't delete automatically. They take up space that could
    //be used otherwise by other programs.
    //syntax of deleting a pointer is
    //delete pointerName;
    delete calc;
    //delete only frees the memory that the value of the pointer says
    //the pointer will still maintain it's value, i.e. the memory address
    //it was assigned to.
    //so it is important to also nullify the pointer's value
    calc = NULL; //calc = 0; is equivalent
    
    printf("Pointer address value: %X\n", calc);
    system("PAUSE");
    return 0;
}






