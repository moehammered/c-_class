#ifndef QUAD__H
#define QUAD__H

#include <SDL_opengl.h> //need this for opengl stuff

class Quad
{
	public:
		float r, g, b; //colour values
		Quad(bool useLocal); //Constructor for the class
		void move(float x, float y, float z);
		void rotate(float x, float y, float z);
		void draw();

	private:
		bool useLocalPosition = false; //whether or not rotations effect translation
		float x, y, z; //variables to store position
		float xRot, yRot, zRot; //rotation variables
		~Quad(); //deconstructor for the class
};

#endif