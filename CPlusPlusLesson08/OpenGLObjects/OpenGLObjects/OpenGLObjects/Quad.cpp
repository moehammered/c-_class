#include "Quad.h"

Quad::Quad(bool useLocal) //Constructor of the class
{
	//initialise all our values to a default value
	//position for this quad is 0, 0, 0
	x = y = z = 0;
	//default rotation for this quad is 0, 0, 0
	xRot = yRot = zRot = 0;
	//colour for this quad is white - 1, 1, 1
	r = g = b = 1;
	//set whether or not to use local translation
	useLocalPosition = useLocal;
}

//works the same way that translate does by adding the values
//to our current position values
void Quad::move(float x, float y, float z)
{
	//is add the value of the parameters to our current position
	this->x += x;
	this->y += y;
	this->z += z;
}

//is expected to work the same way as the rotate function in Unity
//by rotating our object only on it's local axis
void Quad::rotate(float x, float y, float z)
{
	//adding the value to the current rotation values
	this->xRot += x;
	this->yRot += y;
	this->zRot += z;
}

/**
	Will render the quad by doing the following:
	Set the colour
	Set the translation
	Set the rotation
	Draw the primitive
*/
void Quad::draw()
{
	//preserve the matrix stack that already exists
	//this will make sure we don't effect anything else
	glPushMatrix();
		//reset the world to 0, 0, 0
		glLoadIdentity();
		//Set the colour by calling glColor3f(r,g,b);
		glColor3f(r, g, b);
		if (useLocalPosition) //if localPosition, we want to set rotation first, then translate
		{
			//set the rotation by applying each rotation consecutively
			//(one at a time)
			//set the xRotation first by calling glRotatef(xRot, 1, 0, 0);
			glRotatef(xRot, 1, 0, 0);
			//set the yRotation second by calling glRotatef(yRot, 0, 1, 0);
			glRotatef(yRot, 0, 1, 0);
			//set the zRotation third by calling glRotatef(zRot, 0, 0, 1);
			glRotatef(zRot, 0, 0, 1);
			//set the translate by calling glTranslatef(x, y, z);
			glTranslatef(x, y, z);
		}
		else //if we don't, we want to set the translation first, then rotate
		{
			//set the translate by calling glTranslatef(x, y, z);
			glTranslatef(x, y, z);
			//set the rotation by applying each rotation consecutively
			//(one at a time)
			//set the xRotation first by calling glRotatef(xRot, 1, 0, 0);
			glRotatef(xRot, 1, 0, 0);
			//set the yRotation second by calling glRotatef(yRot, 0, 1, 0);
			glRotatef(yRot, 0, 1, 0);
			//set the zRotation third by calling glRotatef(zRot, 0, 0, 1);
			glRotatef(zRot, 0, 0, 1);
		}
		//start drawing the primitive
		//start immediate shape render mode - known as immediate mode
		glBegin(GL_QUADS);
			//accept vertex co-ordinates
			glVertex3f(-0.5f, -0.5f, 0);
			glVertex3f(0.5f, -0.5f, 0);
			glVertex3f(0.5f, 0.5f, 0);
			glVertex3f(-0.5f, 0.5f, 0);
		//end immediate shape render mode
		glEnd();
	//Restore the original matrix that was preserved before object started draw
	glPopMatrix();
}