#include "setup.h"
#include "Quad.h"

Quad* myQuad, *secondQuad;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//OpenGL context
SDL_GLContext gContext;

//Input handler
void handleKeys(unsigned char key, int x, int y);

//starts before the application enters main loop
void start();

//Per frame update
void update();

//Renders quad to the screen
void render();

//Frees media and shuts down SDL
void close();

//draws a quad shape in OpenGL
void drawQuad();

//Render flag
bool gRenderQuad = true;

void start()
{
	enableGLCapabilities();
	//setup first quad
	myQuad = new Quad(false);
	myQuad->move(0, 0, -10);
	//setup second quad
	secondQuad = new Quad(false);
	secondQuad->move(0, 0, -9);
	secondQuad->b = 0;
	secondQuad->g = 0;
}

void handleKeys(unsigned char key, int x, int y)
{
	//Toggle quad
	if (key == 'q')
	{
		gRenderQuad = !gRenderQuad;
	}
}

void update()
{
	//move first quad
	//myQuad->move(-0.01f, 0, -0.1f);
	myQuad->rotate(0, 1.2f, 0);
	//move second quad
	//secondQuad->move(0.01f, 0.01f, -0.01f);
	secondQuad->rotate(0, 1.0f, 0);
}

void render()
{
	//Clear any unneccessary data from previous frame
	//Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	//every frame the world is reset back to 0,0,0
	glLoadIdentity();
	//push the view to be infront of the camera
	glTranslatef(0, 0, -10);

	myQuad->draw();

	secondQuad->draw();
}

bool init(int SCREEN_WIDTH, int SCREEN_HEIGHT)
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Use OpenGL 2.1
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

		//Create window
		gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create context
			gContext = SDL_GL_CreateContext(gWindow);
			if (gContext == NULL)
			{
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Use Vsync
				if (SDL_GL_SetSwapInterval(1) < 0)
				{
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}

				//Initialize OpenGL
				if (!initGL())
				{
					printf("Unable to initialize OpenGL!\n");
					success = false;
				}
			}
		}
	}

	return success;
}

int main(int argc, char* argv[])
{
	if (!init(800, 600))
		return 1;
	//Enable text input
	SDL_StartTextInput();
	bool quit = false;
	SDL_Event e;

	//call the start function here before everything begins
	start();
	//While application is running
	while (!quit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			//Handle keypress with current mouse position
			else if (e.type == SDL_TEXTINPUT)
			{
				int x = 0, y = 0;
				SDL_GetMouseState(&x, &y);
				handleKeys(e.text.text[0], x, y);
			}
		}

		//run the update function
		update();

		//Render quad
		render();

		//Update screen
		SDL_GL_SwapWindow(gWindow);
	}

	//Disable text input
	SDL_StopTextInput();

	return 0;
}