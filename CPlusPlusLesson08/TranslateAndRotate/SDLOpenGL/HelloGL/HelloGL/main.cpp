#include "setup.h"

float zPosition = -10;
float zRotation = 0;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;

//OpenGL context
SDL_GLContext gContext;

//Input handler
void handleKeys(unsigned char key, int x, int y);

//Per frame update
void update();

//Renders quad to the screen
void render();

//Frees media and shuts down SDL
void close();

//draws a quad shape in OpenGL
void drawQuad();

//Render flag
bool gRenderQuad = true;

void drawQuad()
{
	//start immediate shape render mode - known as immediate mode
	glBegin(GL_QUADS);
		//accept vertex co-ordinates
		glVertex3f(-0.5f, -0.5f, 0);
		glVertex3f(0.5f, -0.5f, 0);
		glVertex3f(0.5f, 0.5f, 0);
		glVertex3f(-0.5f, 0.5f, 0);
	//end immediate shape render mode
	glEnd();
}

void handleKeys(unsigned char key, int x, int y)
{
	//Toggle quad
	if (key == 'q')
	{
		gRenderQuad = !gRenderQuad;
	}
	switch (key)
	{
		case 'u':
			zPosition += 0.01f;
			break;
		case 'i':
			zPosition -= 0.01f;
			break;
	}
}

void update()
{
	//No per frame update needed
}

void render()
{
	zRotation += 1.0f;
	//Clear any unneccessary data from previous frame
	//Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);

	//Render quad
	if (gRenderQuad)
	{
		//reset the scene matrix(positions) back to 0,0,0
		glLoadIdentity();
		//move 'into' the scene by 0.01f
		glTranslatef(0, 0, zPosition);
		glColor3f(1.0f, 0, 1.0f);
		//first quad world co-ordinates are 0, 0, zPosition
		drawQuad();
		//move the world position by -2, 0, 0
		glTranslatef(-2, 0, 0);
		//current world position is -2, 0, zPosition
		glColor3f(1, 0.5f, 1);
		drawQuad();
		//move the world position by 0, 2, 0
		glTranslatef(0, 2, 0);
		//current world position is -2, 2, zPosition
		glColor3f(0, 0.5f, 1);
		drawQuad();
		//move the world position by 2, 0, 0
		glTranslatef(2, 0, 0);
		glRotatef(zRotation, 0, 0, 1);
		//current world position is 0, 2, zPosition
		glColor3f(0.5f, 0.5f, 0);
		drawQuad();
	}
}

bool init(int SCREEN_WIDTH, int SCREEN_HEIGHT)
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Use OpenGL 2.1
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

		//Create window
		gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create context
			gContext = SDL_GL_CreateContext(gWindow);
			if (gContext == NULL)
			{
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Use Vsync
				if (SDL_GL_SetSwapInterval(1) < 0)
				{
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}

				//Initialize OpenGL
				if (!initGL())
				{
					printf("Unable to initialize OpenGL!\n");
					success = false;
				}
			}
		}
	}

	return success;
}

int main(int argc, char* argv[])
{
	if (!init(800, 600))
		return 1;
	//Enable text input
	SDL_StartTextInput();
	bool quit = false;
	SDL_Event e;

	//While application is running
	while (!quit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			//Handle keypress with current mouse position
			else if (e.type == SDL_TEXTINPUT)
			{
				int x = 0, y = 0;
				SDL_GetMouseState(&x, &y);
				handleKeys(e.text.text[0], x, y);
			}
		}

		//Render quad
		render();

		//Update screen
		SDL_GL_SwapWindow(gWindow);
	}

	//Disable text input
	SDL_StopTextInput();

	return 0;
}