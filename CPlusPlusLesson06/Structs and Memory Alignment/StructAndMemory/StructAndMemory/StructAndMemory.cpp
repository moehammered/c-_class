// StructAndMemory.cpp : Defines the entry point for the console application.
//

#include "stdafx.h" //what the hell is this file? Find out next time.
#include <cstdlib>
#include <stdio.h>

struct MemoryPadding
{
	double double1;								//double is 8 bytes
	short short1, short2;								//short is 2 bytes
	char var1, var2, var3, var4, var5;			//char is 1 byte, 5
	
											//total size is 17
};

struct CharacterInfo
{
	char* name;			//size of a char* in bytes is 4 
	unsigned int age;			//size of an int in bytes is 4
	bool isMale;		//size of a bool in bytes is 1
						//total size is 9 bytes
};

void printCharacterVariableAddresses(CharacterInfo &reference);
void printCharacterInfo(CharacterInfo info);

//main is named differently??? _tmain... we look this up later
int _tmain(int argc, _TCHAR* argv[])
{
	//this pointer will be used to jump along spaces in memory
	char* memoryHopping = 0;
	//Make a variable using the struct
	CharacterInfo character;
	memoryHopping = (char*)&character; //convert address into char*
	//Initialise the values of the struct
	character.name = "Ummmm";
	character.isMale = false;
	character.age = 300;
	//print the values to the console
	printCharacterInfo(character);
	//print the size of the struct type
	printf("Size of char* is: %i\n", sizeof(char*));
	printf("Size of struct type: %i\n", sizeof(CharacterInfo));
	printf("Size of struct variable: %i\n", sizeof(character));
	printf("Size of struct type: %i\n", sizeof(MemoryPadding));
	//print the size of your variable that uses the struct.
	printf("Address of character var: %X\n", &character);
	printf("Address of memoryHopping pointer: %X\n", memoryHopping);
	printf("memoryHopping dereferenced as a c string: %s\n",( (CharacterInfo*)memoryHopping )->name);
	memoryHopping += 4; //move 4 bytes along
	printf("memoryHopping dereferenced as an integer: %i\n", (unsigned int)*memoryHopping);
	memoryHopping += 4;
	printf("memoryHopping dereferenced as a boolean: %i\n", (bool)*memoryHopping);
	
	
	//print the address of the struct variable you created
	//print the address of each member inside the struct
	//to get the address use the & symbol
	//look at the hex values you get, and notice how they are
	//close to each other.
	printCharacterVariableAddresses(character);
	system("PAUSE");
	return 0;
}

void printCharacterVariableAddresses(CharacterInfo &reference)
{
	printf("Address of Character: %X\n", &reference);
	printf("Address of Character gender: %X\n", &reference.isMale);
	printf("Address of Character name: %X\n", &reference.name);
	printf("Address of Character age: %X\n", &reference.age);
}

void printCharacterInfo(CharacterInfo info)
{
	printf("CharacterName: %s\n", info.name);
	printf("CharacterGender: %i\n", info.isMale);
	printf("CharacterAge: %i\n", info.age);
}