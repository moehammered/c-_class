// FunctionParameters.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cstdlib>
#include <stdio.h>

void printParameterAddress(int value);
void printParameterPointer(int* pointer);
void printParameterReference(int& reference);

int _tmain(int argc, _TCHAR* argv[])
{
	int myAge = 20;
	int* myIntPointer = new int; //make a pointer an assign it memory;
	*myIntPointer = 10;
	printf("Address of myAge variable is: %X\n", &myAge);
	printParameterAddress(myAge);
	printf("After running printParameterAddress for myAge, value is: %i\n", myAge);

	printf("Address of myIntPointer is: %X\n", &myIntPointer);
	printf("Address in myIntPointer is: %X\n", myIntPointer);
	printf("Value at location of myIntPointer is: %i\n", *myIntPointer);
	printParameterPointer(myIntPointer);
	printf("After running printParameterPointer for myIntPointer, value is: %i\n", *myIntPointer);

	printParameterReference(*myIntPointer);
	printf("Value of myIntPointer after running reference function is: %i\n", *myIntPointer);

	printf("Address of printParameterReference is: %X\n", printParameterPointer);

	system("PAUSE");
	delete myIntPointer;
	myIntPointer = 0;
	return 0;
}

void printParameterAddress(int value)
{
	printf("Value of parameter is: %i\n", value);
	printf("Address of parameter is: %X\n", &value);
	value = 300;
}

void printParameterPointer(int* pointer)
{
	//print the address of the pointer
	printf("Address of the pointer is: %X\n", &pointer);
	//print the address the pointer points to(i.e. just print the pointer)
	printf("Address contained inside the pointer is: %X\n", pointer);
	//print the value of the memory location the pointer points to
	printf("Value contained inside the memory location of the pointer is: %i\n", *pointer);
	*pointer = -100;
}

void printParameterReference(int& reference)
{
	//print the address of the reference variable
	printf("Address of the reference variable is: %X\n", &reference);
	//print the value of the reference variable
	printf("Value contained within the reference variable is: %i\n", reference);
	reference = 1005;
}