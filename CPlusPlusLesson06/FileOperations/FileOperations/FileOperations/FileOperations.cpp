// FileOperations.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cstdlib>
#include <stdio.h>

void openFile(char* fileName, char* fileMode);
char* readFile(char* fileName); //returns a pointer to data obtained from a file
void readFileInfo(char* fileBuffer);

int _tmain(int argc, _TCHAR* argv[])
{
	char* fileContents = NULL;
	//openFile("ExampleFile.txt", "a+");
	fileContents = readFile("ExampleFile.txt");
	if (fileContents != NULL)
	{
		readFileInfo(fileContents);
	}
	delete[] fileContents;
	system("PAUSE");
	return 0;
}

char* readFile(char* fileName)
{
	//open the file so it can only be read
	FILE* file = fopen(fileName, "r");
	if (file != NULL) //if we opened it correctly
	{
		int currFilePos = 0; //make this var to store current pos in file
		int fileSize = 0;
		int fSeekReturn = 0;
		//Print notice that file is open
		printf("File is ready to be read!\n");
		//get the current position of the file marker
		currFilePos = ftell(file);
		//print the file marker position
		printf("Current File Marker position is: %i\n", currFilePos);
		//now we tell it to seek the END OF THE FILE
		fSeekReturn = fseek(file, 1, SEEK_END);
		//print if its done successfully
		printf("fSeek return value is: %i\n", fSeekReturn);
		//get the file marker location, which should be at the end of the file
		currFilePos = ftell(file);
		//my file size is however long the file marker travelled within the file
		fileSize = currFilePos;
		//print the 'size' of the file
		printf("Current File Marker position is: %i\n", currFilePos);
		//reset the marker back to the beginning of the file
		//second parameter says how much to offset the marker
		currFilePos = fseek(file, 0, SEEK_SET);
		//OR
		//rewind(file);
		printf("Using SEEK_SET, file marker is now at: %i\n", currFilePos);

		//ready to read the data out of the file and put it into an array
		//make a buffer that can contain the whole file!
		char* fileBuffer = new char[fileSize];
		//we need to read the data into the buffer from the file
		fread(fileBuffer, 1, fileSize, file);
		fileBuffer[fileSize - 1] = '\0'; //Null terminator at end of buffer
		printf("LAST CHARACTER IN FILE BUFFER IS: %i\n", fileBuffer[fileSize-1]);
		printf("File contents contained in the buffer are: \n%s", fileBuffer);
		//close the file, we don't need it anymore
		fclose(file);
		//return the buffer representing the file
		return fileBuffer;
		////free the memory of the buffer, we don't need it anymore
		//delete[] fileBuffer;
		//fileBuffer = 0;
	}

	//file failed to open, return NULL
	return NULL;
}

void openFile(char* fileName, char* fileMode)
{
	char stringBuffer[255];
	FILE* file = fopen(fileName, fileMode);
	if (file != NULL)
	{
		printf("Successfully opened the file!\n");
		fputs("This is the new value I'm putting in!\n", file);
		//puts the current line marker of the file back to the beginning
		rewind(file);
		//get the file contents on the current line of the file
		fgets(stringBuffer, 255, file);
		//print the array containing the file contents
		printf("Data read from file is: \n%s", stringBuffer);
		fclose(file);
	}
}

void readFileInfo(char* fileBuffer)
{
	int currentLine = 1;
	int counter = 0; //represent the index

	while (fileBuffer[counter] != '\0') //while we haven't reached the end
	{
		//get the current character in the file
		char currentChar = fileBuffer[counter];
		switch (currentChar)
		{
			case 'S': //have we found the 'safe value'
				printf("The character is safe!\n");
				break;
			case 'U': //have we found the 'unsafe value'
				printf("The character is unsafe!\n");
				break;

			case '\n':
				currentLine++;
				break;
		}
		if (currentLine == 2) //the second line will represent RANK
		{
			printf("You're rank is: %c\n", currentChar);
		}
		else if (currentLine == 3)
		{
			printf("You're current level is: %i\n", currentChar-48);
		}

		counter++;
	}

	printf("Lines went through in file is: %i\n", currentLine);
}