#include <cstdlib>
#include <stdio.h>

//function prototypes/forward decleration
void printWord(char* word);
char readKeyboard();
bool compareCharacter(char* word, char letter, int position);
void runGame(char* word);


int main()
{
    //this cannot be called unless we use function prototyping
    //also known as 'forward decleration'
    printWord("Welcome to the real input!");
    runGame("hello");
    
    system("PAUSE");
    return 0;
}

void runGame(char* word)
{
    printWord("I'm thinking of a word... Try and guess the first letter!");
    int lives = 3;
    
    while(lives > 0)
    {
        char key = readKeyboard(); //check the keyboard for input
        //check the first letter
        //you can either check == 10 or == '\n'
        //'\n' == 10
        if(key != 10) //if the key is not the 'new line key' (enter/return key)
        {
            //then we should check what they put in as input! :D
            if(compareCharacter(word, key, 0))
            {
                //guessed correctly!
                printWord("You guessed it!");
                printf("It was: %s\n", word);
                printf("You have %i lives left!\n", lives);
                //stop the game by breaking the loop!
                break;
            }
            else
            {
                lives--; //subtract a life from the player
                printWord("You guessed incorrectly!");
                printf("You have %i lives remaining!\n", lives);
            }
        }
    }
}

bool compareCharacter(char* word, char letter, int position)
{
    if(word[position] == letter)
    {
        return true;
    }
    
    return false;
}

char readKeyboard()
{
    printf("Press a key! \n");
    char key = 0;
    //key = getchar();
    scanf("%1c", &key);
    printf("Key pressed: %c\n", key);
    
    return key;
}

void printWord(char* word)
{
     printf("%s\n", word);
}
