/**
   std - Standard and usually when a library has std it means it is a
   standardised library.
                A standardised library means it will function the same
                on any platform which implements it.
   cstdlib = 'C' standard programming library
   stdio = Standard input/output programming library
*/

#include <cstdlib> //included so we can use 'system("PAUSE");'
#include <stdio.h> //included so we can use 'printf'

void printBool(bool value)
{
     if(value == false)
     {
         printf("My Boolean: false\n");
     }
     else
     {
         printf("My Boolean: true\n");
     }
}

void printMemoryLocations(int v1, float v2, double v3, bool v4)
{
    printf("\nPrinting memory locations\n");
    printf("My Number: 0x%X\n", &v1); //%x means hexidecimal number
    printf("My Float: 0x%X\n", &v2); //%x means hexidecimal number
    printf("My Double: 0x%X\n", &v3); //%x means hexidecimal number
    printf("My Boolean: 0x%X\n", &v4); //%x means hexidecimal number
}

void printPointer()
{
    //Syntax of making a pointer variable
    //type '*' name ';'
    //traditionally you should ALWAYS INITIALISE YOUR POINTERS TO NULL!
    //NULL == 0
    //int *myIntPointer = 0 means make me a pointer that doesn't have a 
    //memory location assigned to it yet!
    int *myIntPointer = 0;
    int *secondPointer = 0;
    
    printf("Memory location of my pointer is: 0x%x\n", myIntPointer);

    //assign a new memory location to this pointer that is of size int
    //this is referred to as 'dynamic memory allocation'
    myIntPointer = new int; 
    //secondPointer will have the exact address as myIntPointer
    secondPointer = myIntPointer;
    printf("Memory location of my pointer is: 0x%x\n", myIntPointer); 
    printf("Memory location of my pointer is: 0x%x\n", secondPointer); 
    //*myIntPointer is referred to as 'dereferencing a pointer'
    //dereferencing a pointer basically means give me the value at the 
    //memory location the pointer is assigned to.
    printf("Value at memory location is: %i\n", *myIntPointer);
    
    *myIntPointer = 0; //assign the value of 0 to the value inside the memory
    *secondPointer = -1; //assign the value of -1 to the value inside the memory
    printf("Value at memory location is: %i\n", *myIntPointer);
    
    printf("Value at memory location is: %i\n", secondPointer[0]);
    
    //cleaning up a pointer involves 2 steps
    //step 1: delete the memory of the pointer
    //step 2: re-initialise the pointer back to null (0)
    
    //syntax to delete a pointer
    //delete pointerName;
    
    delete myIntPointer;
    myIntPointer = 0;
    printf("Value at memory location is: %i\n", secondPointer[0]);
}

void printStringProperties(char* word)
{
    int counter = 0; //going to represent the index
    printf("\n\n\n\n\n");
    
    printf("Starting address of string is: 0x%x\n\n", word);
    
    //'\0' is a SINGLE CHARACTER which means 'NULL'
    while(word[counter] != '\0')
    {
        printf("Letter No. %i: %c\n", counter, word[counter]);
        printf("Memory location of letter: 0x%x\n\n", word+counter);
        
        counter++; //to move forward in counter otherwise we'll 
                   //make an infinite loop
    }
    
    printf("Ending address of string is: 0x%x\n\n", word+counter);
}

int main()
{
    //variable types are int, char, bool, float, double, short
    //'string' is represented by using "char*" variableName ;
    //syntax is type name ;
    
    //In C there is a symbol which means "the memory address of a value"
    //This symbol is the ampersand symbol ('&') (shift+7)
    
    int myNumber = 0;
    char *myWord = "Word"; //char* is a 'c-style string'
    float myFloat = 0.5f;
    double myDouble = 2.3;
    bool myBoolean = 0;
    
    printf("My Number: %i\n", myNumber);
    printf("My Word: %s\n", myWord);
    printf("My Float: %f\n", myFloat);
    printf("My Double: %f\n", myDouble);
    //printf("My Boolean: %i\n", myBoolean);
    printBool(myBoolean);
    
    printMemoryLocations(myNumber, myFloat, myDouble, myBoolean);
   
    printf("\n\nMemory location of my string is: 0x%x\n\n", myWord);
    printf("First Letter: %c\n", myWord[0]);
    //alternative way of finding memory location '&(myWord[0])' (UGLY)
    printf("First Letter Location: 0x%x\n\n", myWord+0 ); 
    
    printf("Second Letter: %c\n", myWord[1]);
    printf("Second Letter Location: 0x%x\n\n", myWord+1);
    
    printf("Value at memory location: %i\n", (&myNumber+1));
   
    printPointer();
    
    printStringProperties("Run");
   
    system("PAUSE");
    return 0;
}







