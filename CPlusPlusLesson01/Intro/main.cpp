/*#include <cstdlib>
#include <iostream>

using namespace std; //explain this later

int main(int argc, char *argv[])
{
    printf("Hello World!\n");
    system("PAUSE");
    return EXIT_SUCCESS;
}*/

#include <cstdlib>
#include <iostream>
#include "conio.h"

void printArgumentParameters(int argc, char* argv[])
{
    //print argument count ("\n means ")
    printf("Argument count: %i\n", argc);
    //print first argument value
    printf(argv[0]);
    
    if(argc > 1)
    {
            printf("\nYou gave me more arguments!\n");
            for(int i = 1; i < argc; i++)
            {
                    printf("\nArgument No. %i\n", i);
                    printf(argv[i]);
            }
    }
}

void printWord(char* string, int characterNo)
{
     char* myString = string;
     printf("Letter in my word: %c\n", myString[characterNo]);
}

void argumentChecker(int argc, char *argv[])
{
     for(int i = 1; i < argc; i++)
     {
             //check each words first character
             if(argv[i][0] == '-')
             {
                  //parameter found, check letter for command
                  if(argv[i][1] == 'h')
                  {
                       printf("\nHello!\n");
                  } 
                  else if(argv[i][1] == 'f')
                  {
                       printf("\nFuck!\n");
                  }
             }
     }
}

int main(int argc, char *argv[])
{
    printArgumentParameters(argc, argv);
    
    //go to new line
    printf("\n");
    
    printWord("Hello again!", 2);
    
    printf("\n");
    
    argumentChecker(argc, argv);
    
    while(1)
    {
        if(kbhit())
        {
            printf("Key hit: %c\n", getch());
        }
    }
    
    //pause the application
    system("PAUSE");
    return 0;
}













